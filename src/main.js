import Vue from 'vue'

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import Vuetify from 'vuetify/lib'
import 'vuetify/dist/vuetify.min.css'
Vue.use(Vuetify)

import axios from "axios"
import VueAxios from "vue-axios"
Vue.use(VueAxios, axios)

import store from "./store/store.js";

import App from './App.vue'
import Game from './Game.vue'
import Lobby from './Lobby.vue'

const routes = [
  {
    path: "/:id",
    name: "game",
    component: Game
  },
  {
    path: "/:id/lobby",
    name: "lobby",
    component: Lobby,
  }
];

const router = new VueRouter({
  mode: "history",
  routes
});

new Vue({
  el: '#app',
  router,
  render: h => h(App),
  store,
  vuetify: new Vuetify({theme:{dark:true}})
})
