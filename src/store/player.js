const state = {
  name: null,
  hand: [],
  id: null,
  position: null
};

const getters = {
  hand (state) {
    return state.hand
  },
  id (state) {
    return state.id
  },
  player (state) {
    return state
  },
  position (state) {
    return state.position
  }
};

const actions = {
  update ({ commit }, p) {
    commit("update", p)
  }
};

const mutations = {
  update (state, p) {
    Object.assign(state, p)
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
