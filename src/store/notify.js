const defaultOpts = {
  closable: true,
  color: "grey darken-3",
  msg: "You should not see this message, please report it.",
  show: false,
  timeout: 0
};

const state = {
  closable: true,
  color: "",
  msg: "",
  show: false,
  timeout: 0
};

const getters = {
  closable (state) {
    return state.closable;
  },
  color (state) {
    return state.color;
  },
  msg (state) {
    return state.msg;
  },
  show (state) {
    return state.show;
  },
  timeout (state) {
    return state.timeout;
  }
};

const actions = {

  notify ({ commit }, options) {

    const {
      closable,
      color,
      msg,
      timeout
    } = Object.assign({}, defaultOpts, options);

    commit("setClosable", closable);
    commit("setColor", color);
    commit("setMsg", msg);
    commit("setTimeout", timeout);
    commit("show");

  },

  acknowledge (context) {
    context.commit("hide");
  }

};

const mutations = {
  hide (state) {
    state.show = false;
    state.timeout = 0;
  },
  setClosable (state, closable) {
    state.closable = closable;
  },
  setColor (state, color) {
    state.color = color;
  },
  setMsg (state, msg) {
    state.msg = msg;
  },
  setTimeout (state, timeout) {
    state.timeout = timeout;
  },
  show (state) {
    state.show = true;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
