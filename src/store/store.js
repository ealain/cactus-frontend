import Vue from "vue";
import Vuex from "vuex";

import player from "./player.js";
import notify from "./notify.js";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: [
    player,
    notify
  ]
});
