**Cactus** is a card game for two or more players, in which the object is to score as little as possible.

## Quick start (frontend)

```
npm install
npm run dev
```
