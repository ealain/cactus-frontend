const path = require('path')
const webpack = require('webpack')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const HtmlWebpackPlugin = require("html-webpack-plugin")
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/',
    filename: 'build.js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      favicon: "./src/favicon.ico",
      title: "Cactus",
      template: "./src/index.html"
    }),
    new webpack.DefinePlugin(
      { VERSION: JSON.stringify(require("./package.json").version) }
    ),
    new VueLoaderPlugin(),
    // new BundleAnalyzerPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        },
        exclude: /node_modules/
      }
    ]
  },
  devServer: {
    historyApiFallback: true,
    host: '0.0.0.0',
    noInfo: true,
    overlay: {
      errors: true,
      warnings: true
    },
    proxy: {
        '/api': {
            target: 'http://localhost:8081',
            ws: true
        }
    }
  },
  performance: {
    // hints: 'warning'
  }
}
